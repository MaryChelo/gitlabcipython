# Doctest - Python

## Introducción

Este repositorio muestra la ejecución de las pruebas mediante Doctest utilizando Python.

## Pruebas

```python
import doctest

def factorial(n):
    """
    >>> factorial(3)
    6
    >>> factorial(4)
    24
    >>> factorial(5)
    120
    """
    return 1 if n < 1 else n * factorial(n-1)


if __name__ == "__main__":
    doctest.testmod()

```

## Ejectuar pruebas

```shell
python factorial.py -v
```

## Resultado

```shell
usuario$ python factorial.py -v
Trying:
    factorial(3)
Expecting:
    6
ok
Trying:
    factorial(4)
Expecting:
    24
ok
Trying:
    factorial(5)
Expecting:
    120
ok
1 items had no tests:
    __main__
1 items passed all tests:
   3 tests in __main__.factorial
3 tests in 2 items.
3 passed and 0 failed.
Test passed.

```

## Datos

CIMAT,AC - Unidad Zacatecas

Maestría en Ingeniería de Software 

**Actividad**

Doctest - Python

**Matería**

Aseguramiento de la cálidad

**Profesor**

Alejandro García Fernández

**Equipo**

* Nephtali Ceballos
* Marichelo García 
* Luis Angel Pinedo 
* Jorge Sánchez
