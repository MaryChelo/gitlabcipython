import doctest

def dictionary_mapping(names, id2salaries):
    """
    (Task 0.5.26) A dictionary mapping names to salaries
    >>> dictionary_mapping(['Larry', 'Curly', 'Moe'],{0:1000.0, 1:1200.50, 2:990})
    {'Larry': 1000.0, 'Moe': 990, 'Curly': 1200.5}    
    """
    
    val = {}
    for i in range(0, len(names)):
        val[names[i]] = id2salaries[i]
    
    return val

if __name__ == "__main__":
    doctest.testmod()
    
