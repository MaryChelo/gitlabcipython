import doctest
def reminder_without_mod(numerator, divisor):
    """
    2: (Task 0.5.2) Remainder
    >>> reminder_without_mod(28,7)
    0
    >>> reminder_without_mod(30,7)
    2
    """
    return numerator%divisor

if __name__ == "__main__":
    doctest.testmod()

