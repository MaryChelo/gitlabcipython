import doctest

def value_list(k, dlist):
    """
    21.(Task 0.5.21) Extracting the value corresponding to key k from each dictionary in a list
    >>> value_list('James',[{'James':'Sean', 'director':'Terence'}, {'James':'Roger', 'director':'Lewis'}, {'James':'Pierce', 'director':'Roger'}])
    ['Sean', 'Roger', 'Pierce']
    """
    val=[]
    for i in dlist:
    	val.append(i[k])
    return val

if __name__ == "__main__":
    doctest.testmod()
