import doctest


def LofL_sum(list_of_lists):
    """
    12: (Task 0.5.12) Sum of numbers in list of list of numbers
    a one-line expression of the form sum([sum(...) ... ]) that
    includes a comprehension and evaluates to the sum of all numbers in all
    the lists.
    >>> LofL_sum([[.25, .75, .1], [-1, 0], [4, 4, 4, 4]])
    16.1
    """
    total = 0
    for lista in list_of_lists:
        total += sum(lista)
    return total


if __name__ == "__main__":
    doctest.testmod()
