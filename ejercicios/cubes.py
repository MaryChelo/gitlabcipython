import doctest
def cubes(L):
    """
    Task 0.5.29: Define a one-line procedure cubes(L) specified as follows:
    - input: list L of numbers
    - output: list of numbers whose ith element is the cube
              of the ith element of L
    >>> cubes([2, 3, 4])
    [8, 27, 64]
    """
    return [ x**3 for x in L]

if __name__ == "__main__":
    doctest.testmod()
